import { Routes, RouterModule } from "@angular/router";

import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { RestoreComponent } from "./restore/restore.component";
import { HomeComponent } from "./home/home.component";
import { AuthGuard } from "./auth.guard";

const appRoutes: Routes = [
    { path: "login", component: LoginComponent },
    { path: "register", component: RegisterComponent },
    { path: "restore", component: RestoreComponent },
    { path: "", component: HomeComponent, canActivate: [AuthGuard] },
    { path: "**", redirectTo: "" }
];

export const routing = RouterModule.forRoot(appRoutes);