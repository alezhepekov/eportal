import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
// !!! Test backend !!!
// import { testBackendProvider } from "./test.backend";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { BaseRequestOptions } from "@angular/http";
import { AppComponent } from "./app.component";
import { routing } from "./app.routing";
import { AuthGuard } from "./auth.guard";
import { AuthService } from "./auth.service";
import { UserService } from "./user.service";
import { BlogService } from "./blog.service";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { RestoreComponent } from "./restore/restore.component";
import { HomeComponent } from "./home/home.component";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
        RestoreComponent,
        HomeComponent
    ],
    providers: [
        AuthGuard,
        AuthService,
        UserService,
        BlogService,
        // testBackendProvider,
        MockBackend,
        BaseRequestOptions
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
