import { Component } from "@angular/core";
import { ElementRef } from "@angular/core";

import { BlogService } from "./blog.service";
import { Post } from "./post"

@Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.css"]
})
export class AppComponent {  
	posts: Post[];

	constructor(
		private elementRef:ElementRef,
		private blogService: BlogService) {
	};

	ngOnInit() {
		/*
		this.blogService.posts()
			.subscribe(result => {
				this.posts = result;
			});
		*/
	}
}
