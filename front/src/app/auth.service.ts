import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import { User } from "./user";

@Injectable()
export class AuthService {
    public token: string;

    constructor(private http: Http) {     
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        this.token = currentUser && currentUser.token;
    }

    login(userID: string, passwordHash: string): Observable<boolean> {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("Accept", "application/json");
        return this.http.post("http://localhost:4000/api/auth/login", JSON.stringify({ userID: userID, passwordHash: passwordHash }), { headers: headers })
            .map((response: Response) => {            
                let token = response.json() && response.json().token;
                if (token) {                 
                    this.token = token;
                    localStorage.setItem("currentUser", JSON.stringify({ userID: userID, token: token }));
                    return true;
                } else {                   
                    return false;
                }
            });
    }

    logout(): void {      
        this.token = null;
        localStorage.removeItem("currentUser");
    }

    register(user: User): Observable<boolean> {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("Accept", "application/json");
        return this.http.post("http://localhost:4000/api/auth/register", JSON.stringify(user), { headers: headers })
            .map((response: Response) => {
                console.log(response.json());
                return true;
            });
    }

    restore(userID: string): Observable<boolean> {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("Accept", "application/json");
        return this.http.post("http://localhost:4000/api/auth/restore", JSON.stringify({ userID: userID }), { headers: headers })
            .map((response: Response) => {
                console.log(response.json());
                return true;
            });
    }
}
