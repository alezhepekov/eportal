import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Md5 } from 'ts-md5/dist/md5';

import { AuthService } from '../auth.service';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    error = '';

    constructor(
        private router: Router,
        private authService: AuthService) { }

    ngOnInit() {       
        this.authService.logout();
    }

    login() {
        this.loading = true;
        let userID = this.model.userid;
        let md5 = new Md5();      
        md5.appendStr(this.model.password);
        let hash = md5.end(); 
        let passwordHash: string = hash as string;
        this.authService.login(userID, passwordHash)
            .subscribe(result => {
                if (result === true) {
                    this.router.navigate(['/']);
                } else {
                    this.error = 'Идентификатор пользователя и/или пароль не распознаны';
                    this.loading = false;
                }
            });
    }
}
