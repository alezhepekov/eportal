export class User {
    id: string;
    name: string;
    email: string;
    phone: string;
    fb: string;
    gplus: string;
    youtube: string;
    twitter: string;
    vk: string;
    gitlab: string;
    github: string;
    website: string;
    company: string;
    birthdate: string;
    passwordHash: string;
    role: object;
}
