import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Md5 } from "ts-md5/dist/md5";

import { AuthService } from "../auth.service";
import { User } from "../user";

@Component({
    moduleId: module.id,
    templateUrl: "register.component.html"
})

export class RegisterComponent implements OnInit {
    model: any = {};
    loading = false;
    error = "";

    constructor(
        private router: Router,
        private authService: AuthService) { }

    ngOnInit() {       
    }

    register() {
        this.loading = true;
        let user = new User();
        user.name = this.model.name;
        user.email = this.model.email;
        user.phone = this.model.phone;

        // DEDUG role: admin
        user.role = {
            id: "71164bb8-bd80-4d10-bd04-8d18c2f90251"
        };

        if (this.model.password !== this.model.passwordConfirm) {
            this.error = "Пароли не совпадают";
            this.loading = false;
            return;
        }
      
        let md5 = new Md5();      
        md5.appendStr(this.model.password);
        let hash = md5.end(); 
        let passwordHash: string = hash as string;

        user.passwordHash = passwordHash;
        
        this.authService.register(user)
            .subscribe(result => {
                if (result === true) {
                    this.router.navigate(["/"]);
                } else {
                    this.error = "Ошибка регистрации нового пользователя";
                    this.loading = false;
                }
            });
    }
}
