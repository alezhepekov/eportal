import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";

export function testBackendFactory(backend: MockBackend, options: BaseRequestOptions) {
    backend.connections.subscribe((connection: MockConnection) => {
        let testUser = {
            id: "",
            name: "",
            email: "user@site.net",
            phone: "",
            fb: "",
            gplus: "",
            youtube: "",
            twitter: "",
            vk: "",
            gitlab: "",
            github: "",
            website: "",
            company: "",
            birthdate: "",
            passwordHash: "bf3e452a2f9b7d658bc889b71a6fb7fa",
            role: {
                id: "71164bb8-bd80-4d10-bd04-8d18c2f90251"
            }
        }

        setTimeout(() => {
            if (connection.request.url.endsWith("/api/auth/login") && connection.request.method === RequestMethod.Post) {               
                let loginData = JSON.parse(connection.request.getBody());
                if (
                    (loginData.userid === testUser.email ||
                    loginData.userid === testUser.phone) &&
                    loginData.passwordHash === testUser.passwordHash) {
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200, body: { token: "test-jwt-token" } })
                    ));
                } else {
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200 })
                    ));
                }
            }
           
            if (connection.request.url.endsWith("/api/users") && connection.request.method === RequestMethod.Get) {               
                if (connection.request.headers.get("Authorization") === "Bearer test-jwt-token") {
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200, body: [testUser] })
                    ));
                } else {                   
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 401 })
                    ));
                }
            }

        }, 500);

    });

    return new Http(backend, options);
}

export let testBackendProvider = {   
    provide: Http,
    useFactory: testBackendFactory,
    deps: [MockBackend, BaseRequestOptions]
};