import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Md5 } from 'ts-md5/dist/md5';

import { AuthService } from '../auth.service';

@Component({
    moduleId: module.id,
    templateUrl: 'restore.component.html'
})

export class RestoreComponent implements OnInit {
    model: any = {};
    loading = false;
    error = '';

    constructor(
        private router: Router,
        private authService: AuthService) { }

    ngOnInit() {       
    }

    restore() {
        this.loading = true;
        let userID = this.model.userid;        
        this.authService.restore(userID)
            .subscribe(result => {
                if (result === true) {
                    this.router.navigate(['/']);
                } else {
                    this.error = 'Ошибка восстановления аккаунта';
                    this.loading = false;
                }
            });
    }
}
