import { EportalPage } from './app.po';

describe('eportal App', () => {
  let page: EportalPage;

  beforeEach(() => {
    page = new EportalPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
