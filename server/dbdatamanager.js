const config = require("./config");
var mysql = require("mysql");

function DBDataManager() {
    var pool = mysql.createPool({
        host: config.DB.host,
        user: config.DB.user,
        password: config.DB.password,
        database: config.DB.database
    });

    this.pool = pool;   
}

/* users */

DBDataManager.prototype.selectUserList = function(cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "SELECT * FROM `users`";
        connection.query(sql, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    });   
}

DBDataManager.prototype.selectUser = function(id, cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "SELECT * FROM `users` WHERE `id` = ?";
        var args = [id];
        connection.query(sql, args, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    });  
}

DBDataManager.prototype.selectUserByEmail = function(email, cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "SELECT * FROM `users` WHERE `email` = ?";
        var args = [email];
        connection.query(sql, args, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    });  
}

DBDataManager.prototype.selectUserByPhone = function(phone, cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "SELECT * FROM `users` WHERE `phone` = ?";
        var args = [phone];
        connection.query(sql, args, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    });  
}

DBDataManager.prototype.insertUser = function(data, cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "INSERT INTO `users`(`id`, `name`, `email`, `phone`, `fb`, `gplus`, `youtube`, `twitter`, `vk`, `gitlab`, `github`, `website`, `company`, `passwordhash`, `roleid`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        var args = [data.id, data.name, data.email, data.phone, data.fb, data.gplus, data.youtube, data.twitter, data.vk, data.gitlab, data.github, data.website, data.company, data.passwordHash, data.role.id];
        connection.query(sql, args, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    }); 
}

DBDataManager.prototype.updateUser = function(id, data, cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "UPDATE `users` SET `id`=?, `name`=?, `email`=?, `phone`=?, `fb`=?, `gplus`=?, `youtube`=?, `twitter`=?, `vk`=?, `gitlab`=?, `github=?`, `website`=?, `company`=?, `passwordhash`=?, `roleid`=? WHERE `id`=?";
        var args = [data.id, data.name, data.email, data.phone, data.fb, data.gplus, data.youtube, data.twitter, data.vk, data.gitlab, data.github, data.website, data.company, data.passwordHash, data.role.id, id];
        connection.query(sql, args, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    }); 
}

DBDataManager.prototype.deleteUser = function(id, cb) {
    this.pool.getConnection(function(error, connection) {
        if (error) {          
            return cb && cb(null, error);
        }

        var sql = "DELETE * FROM `users` WHERE `id` = ?";
        var args = [id];
        connection.query(sql, args, (error, result, fields) => {
            connection.release();
            if (error) {          
                return cb && cb(null, error);
            }            

            cb && cb(result);
        });
    });
}

module.exports.DBDataManager = DBDataManager;