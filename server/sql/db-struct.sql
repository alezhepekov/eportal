CREATE TABLE `roles` (
	`id` CHAR(36) CHARACTER SET utf8 NOT NULL,
	`name` VARCHAR(512) CHARACTER SET utf8 NOT NULL,	
	`permissions` VARCHAR(4096) CHARACTER SET utf8 NOT NULL,
	CONSTRAINT PRIMARY KEY (`id`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
	`id` CHAR(36) CHARACTER SET utf8 NOT NULL,
	`name` VARCHAR(512) CHARACTER SET utf8 NOT NULL, 
	`email` VARCHAR(512) CHARACTER SET utf8 NOT NULL,
	`phone` VARCHAR(512) CHARACTER SET utf8,
	`fb` VARCHAR(512) CHARACTER SET utf8,
	`gplus` VARCHAR(512) CHARACTER SET utf8,
	`youtube` VARCHAR(512) CHARACTER SET utf8,
	`twitter` VARCHAR(512) CHARACTER SET utf8,
	`vk` VARCHAR(512) CHARACTER SET utf8,
	`gitlab` VARCHAR(512) CHARACTER SET utf8,
	`github` VARCHAR(512) CHARACTER SET utf8,
	`website` VARCHAR(512) CHARACTER SET utf8,
	`company` VARCHAR(512) CHARACTER SET utf8,
	`birthdate` TIMESTAMP,
	`passwordhash` VARCHAR(512) CHARACTER SET utf8 NOT NULL,
	`roleid` CHAR(36) CHARACTER SET utf8 NOT NULL,
	CONSTRAINT PRIMARY KEY (`id`),
	CONSTRAINT users_unique_email UNIQUE (`email`),
	CONSTRAINT users_unique_phone UNIQUE (`phone`),
	CONSTRAINT FOREIGN KEY fk_users_roleid_to_roles (`roleid`) REFERENCES `roles`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE INDEX idx_users_name ON `users` (`name`);
CREATE INDEX idx_users_email ON `users` (`email`);
CREATE INDEX idx_users_phone ON `users` (`phone`);
CREATE INDEX idx_users_fb ON `users` (`fb`);
CREATE INDEX idx_users_gplus ON `users` (`gplus`);
CREATE INDEX idx_users_youtube ON `users` (`youtube`);
CREATE INDEX idx_users_twitter ON `users` (`twitter`);
CREATE INDEX idx_users_vk ON `users` (`vk`);
CREATE INDEX idx_users_gitlab ON `users` (`gitlab`);
CREATE INDEX idx_users_github ON `users` (`github`);
CREATE INDEX idx_users_website ON `users` (`website`);
CREATE INDEX idx_users_company ON `users` (`company`);
CREATE INDEX idx_users_birthdate ON `users` (`birthdate`);

CREATE TABLE `categories` (
	`id` CHAR(36) CHARACTER SET utf8 NOT NULL,	
	`title` VARCHAR(512) CHARACTER SET utf8 NOT NULL,
	`data` VARCHAR(4096),
	`ts` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	CONSTRAINT PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE INDEX idx_categories_title ON `categories` (`title`);

CREATE TABLE `posts` (
	`id` CHAR(36) CHARACTER SET utf8 NOT NULL,
	`userid` CHAR(36) CHARACTER SET utf8 NOT NULL,
	`categoryid` CHAR(36) CHARACTER SET utf8 NOT NULL,
	`tags` VARCHAR(1024) CHARACTER SET utf8 NOT NULL,
	`title` VARCHAR(512) CHARACTER SET utf8 NOT NULL,
	`data` VARCHAR(4096),
	`ts` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	CONSTRAINT PRIMARY KEY (`id`),
	CONSTRAINT FOREIGN KEY fk_posts_userid_to_users (`userid`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT FOREIGN KEY fk_posts_categoryid_to_categories (`categoryid`) REFERENCES `categories`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE INDEX idx_posts_title ON `posts` (`title`);

CREATE TABLE `comments` (
	`id` CHAR(36) CHARACTER SET utf8 NOT NULL,
	`userid` CHAR(36) CHARACTER SET utf8 NOT NULL,
	`postid` CHAR(36) CHARACTER SET utf8 NOT NULL,
	`parentid` CHAR(36) CHARACTER SET utf8 NULL,
	`data` VARCHAR(4096),
	`ts` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	CONSTRAINT PRIMARY KEY (`id`),
	CONSTRAINT FOREIGN KEY fk_comments_userid_to_users (`userid`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT FOREIGN KEY fk_comments_postid_to_posts (`postid`) REFERENCES `posts`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT FOREIGN KEY fk_comments_parentid_to_comments (`parentid`) REFERENCES `comments`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE INDEX idx_comments_comment_id ON `comments` (`parentid`);

CREATE TABLE `likes` (
	`id` CHAR(36) CHARACTER SET utf8 NOT NULL,
	`userid` CHAR(36) CHARACTER SET utf8 NOT NULL,
	`postid` CHAR(36) CHARACTER SET utf8 NOT NULL,
	`data` VARCHAR(4096),
	`ts` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	CONSTRAINT PRIMARY KEY (`id`),
	CONSTRAINT FOREIGN KEY fk_likes_userid_to_users (`userid`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT FOREIGN KEY fk_likes_postid_to_posts (`postid`) REFERENCES `posts`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
