const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const uuidV4 = require("uuid/v4");
const jwt = require("jsonwebtoken");

const dbdatamanager = require("./dbdatamanager");
var dbDataManager = new dbdatamanager.DBDataManager();

function isValidPhone( val ) {
	var pattern = new RegExp( /^\+?\d{1,15}$/ ); // ITU-T E.164
	return pattern.test( val );
}

function isValidEmail( val ) {
	var pattern = new RegExp( /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i );
	return pattern.test( val );
}

router.use(function (req, res, next) {   
	next();
});

router.use(bodyParser.json());

router.post("/register", function(req, res) {
    var ftx = function(req, res) {
        if (!req.body) {
            res.status(400).end(JSON.stringify({}));
        }
        
        var data = req.body;

        if ( !data.id ) {
            data.id = uuidV4();
        }            
      
        dbDataManager.insertUser(data, (result, error) => {
            if ( error ) {
               return res.status(500).end(JSON.stringify({}));
            }

            res.status(201).end(JSON.stringify({id: data.id}));
        });
    };

    ftx(req, res);
});

router.post("/login", function(req, res) {
    var ftx = function(req, res) {
        if (!req.body) {
            res.status(400).end(JSON.stringify({}));
        }
        
        var data = req.body;
        
        if ( isValidEmail( data.userID ) ) {
            dbDataManager.selectUserByEmail(data.userID, (result, error) => {
                if ( error ) {
                   return res.status(500).end(JSON.stringify({}));
                }

                if ( result.length === 0 ) {
                    return res.status(404).end(JSON.stringify({}));
                }
    
                if (result.paswordhash.toUpper() !== data.paswordHash.toUpper()) {
                    return res.status(403).end(JSON.stringify({}));
                }
    
                jwt.sign({
                        exp: Math.floor(Date.now() / 1000) + (60 * 60), // Время действия 1 час
                        data: user.email
                    },
                    "secret",
                    {},
                    (error, token) => {
                        if ( error ) {
                            return res.status(500).end(JSON.stringify({}));
                        }
    
                        return res.status(200).end(JSON.stringify({token: token}));                    
                    }
                );
            });
        }
        else if ( isValidPhone( data.userID ) ) {
            dbDataManager.selectUserByPhone(data.userID, (result, error) => {
                if ( error ) {
                   return res.status(500).end(JSON.stringify({}));
                }
    
                if (result.paswordhash.toUpper() !== data.paswordHash.toUpper()) {
                    return res.status(403).end(JSON.stringify({}));
                }
    
                jwt.sign({
                        exp: Math.floor(Date.now() / 1000) + (60 * 60), // Время действия 1 час
                        data: user.email
                    },
                    "secret",
                    {},
                    (error, token) => {
                        if ( error ) {
                            return res.status(500).end(JSON.stringify({}));
                        }
    
                        return res.status(200).end(JSON.stringify({token: token}));                    
                    }
                );
            });
        }
        else {
            return res.status(403).end(JSON.stringify({}));
        }        
    };

    ftx(req, res);
});

router.post("/logout", function(req, res) {
    var ftx = function(req, res) {
        res.status(200).end(JSON.stringify({}));
    };

    ftx(req, res);
});

router.post("/restore", function(req, res) {
    var ftx = function(req, res) {
        if (!req.body) {
            res.status(400).end(JSON.stringify({}));
        }
        
        var data = req.body;
        
        if ( isValidEmail( data.userID ) ) {
            dbDataManager.selectUserByEmail(data.userID, (result, error) => {
                if ( error ) {
                   return res.status(500).end(JSON.stringify({}));
                }                
    
				// TODO Генерация и отправка кода восстановления
				
				res.status(200).end(JSON.stringify({}));
            });
        }
        else if ( isValidPhone( data.userID ) ) {
            dbDataManager.selectUserByPhone(data.userID, (result, error) => {
                if ( error ) {
                   return res.status(500).end(JSON.stringify({}));
                }               
    
				// TODO Генерация и отправка кода восстановления

				res.status(200).end(JSON.stringify({}));
            });
        }
        else {
            return res.status(403).end(JSON.stringify({}));
        }       
    };

    ftx(req, res);
});

module.exports = router;
