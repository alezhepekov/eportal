const express = require("express");
const app = express();
const uuidV4 = require("uuid/v4");
const config = require("./config");
const port = process.env.PORT || config.Port || 4000;
const auth = require("./auth");
const users = require("./users");

app.use(function (req, res, next) {    
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, X-Requested-With");
    res.header("Content-Type", "application/json");
  
    if ( req.method === "OPTIONS" ) {
        return res.status(200).end();
    }  

	next();
});

app.use("/api/auth", auth);
app.use("/api/users", users);

app.listen(port);
console.log("Server started " + "127.0.0.1:" + port);